public class Car {
    //A class is the code that describes the object.
    //An instance is a tangible copy of an idea instantiated or created from a class

    //Attributes and Methods
    // public - the variable in the class is accessible anywhere in the application
    //Class attribute/properties must not be public, they should only be accessed and set via class methods called setters and getters.
    //private - limit the access and ability to set a variable/method to its class.
    //setter are public methods which will allow us set the value of an instance
    //getters are public methods which will allow us to get the value of an instance
    private String make;
    private String brand;
    private int price;

    //Add a driver variable to add a property which is an object/instance of a Driver class
    private Driver carDriver;

    //Methods are functions of an object which allows us to perform certain tasks
    //void - means that the function does not return anything. Because in Java, a function/methods' return dataType must be declared.

    public void start(){
        System.out.println("BRUUUUU BURRRRRRRUUUU");
    }

    //parameters in Java needs its dataType declared
//    public void setMake(String makeParams){
//        //this keyword refers to the object where the constructor or setter is
//        this.make = makeParams;
//    }
//
//    public String getMake(){
//        return this.make;
//    }
//
//    public void setBrand(String makeParams){
//        this.brand = makeParams;
//    }
//
//    public String getBrand(){
//        return this.brand;
//    }
//    public void setPrice(int makeParams){
//        this.price = makeParams;
//    }
//
//    public int getPrice(){
//        return this.price;
//    }
    public int getPrice(){
        return price;
    }

    //constructor is a method which allows us to set the initial values of an instance.
    //empty/default constructor - default constructor - allows us to create an instance with default initialized values for our properties

    //THIS IS A CONSTRUCTOR WHICH IS a DEFAULT. THIS IS AUTOMATICALLY CREATED WHEN YOU DON'T HAVE ANY CONSTRUCTOR IN THE CLASS
    public Car(){
//        //empty or you can designate default values instead of getting them from parameters
//        //Java, actaully already creates one for us, however, empty/default constructor made just by Java allows Java to se its own default values. If you create your won, you can create your own defaul values
    }

    //parameterized constructor - allows us to initialize values to our attributes upon creation of the instance
//    public Car(String make, String brand, int price){
//        this.make = make;
//        this.brand = brand;
//        this.price = price;
//    }

    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver = driver;
    }

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    public String getCarDriverName(){
        //carDriver is a class with a method getName
        //The getName then can share the name
        return this.carDriver.getName();
    }
}
