public class Main {
    //public String sample = "sample";

    public static void main(String[] args) {
        //Creating a new instance of the class Main
        //Main newSample = new Main();
        //System.out.println(newSample.sample);

//        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//        car1.make = "Veyron";
//        car1.brand = "Bugatti";
//        car1.price = 2000000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);

        //Each instance of a class should be independent from one another especially their properties. However, since coming from the same class, they may have the same methods
//        Car car2 = new Car();
//        car2.make = "Tamaraw FX";
//        car2.brand = "Toyota";
//        car2.price = 450000;
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);
        //car2.owner = "Mang Hose"; //We cannot add a new property not described in the class

//        Car car3  = new Car();
//        car3.make = "Fortuner";
//        car3.brand = "Toyota";
//        car3.price = 2450000;
//        System.out.println(car3.brand);
//        System.out.println(car3.make);
//        System.out.println(car3.price);
//        car1.start();
//        car2.start();
//        car3.start();
//        car1.setMake("Veyron");
//        System.out.println(car1.getMake());
        //When create a new instance of a class:
        //new keyword allows us to create a new instance
        //Car() - constructor of our class - without arguements - default consturctor is used: which JAVA CAN DEFINED FOR US
        //Car(XXX,XXX,XXX,XXX) - is called a parameterized constructor
        Driver driver1 = new Driver("Sean Mira", "Makati City", 30);
        System.out.println(driver1.getName());
        System.out.println(driver1.getAddress());
        System.out.println();

        Car car4 = new Car();
        Car car5 = new Car("Corolla", "Toyota", 500000, driver1);

        System.out.println(car5.getCarDriver().getName());
        System.out.println(car5.getCarDriverName());

        //Animal activity
        Animal animal1 = new Animal("Askie","white");
        animal1.call();
    }
}